use Term::ANSIColor;
use Switch;

sub check_color { # Checks if the color is valid
  if (not length $_[0]) {
    return ''; # Return '' (false) if $_[0] is undefined or 0 length
  } else {
    foreach $i (@colors) { #
      if ($_[0] eq $i) {
        return '1';
      }
    }
    return '';
  }
}
sub begins_with {
    return substr($_[0], 0, length($_[1])) eq $_[1];
}

# Everything between here and ROOMLOOP: while $loop_state is setup for the room
@colors = ("red", "green", "yellow", "blue", "magenta", "cyan", "white", "black");
my $colorbool = 0;
my $bright_prefix = "bright_";
my ($default, $reset) = ("white", 'reset');
my ($debug_flag, $input_flag, $speech_flag, $game_flag, $error_flag) = ("-d", "-i", "-s", "-g", "-e");
my ($debug_color, $input_color, $speech_color, $game_color, $error_color) = ($default, $default, $default, $default, $default);
my $loop_state = 1;
foreach $item (@ARGV) {
  if (begins_with($item, $debug_flag)) {
    if (check_color (substr($item, 2))) {
      $debug_color = substr($item, 2);
      $debug_color = "$bright_prefix$debug_color";
    }
  } elsif (begins_with($item, $input_flag)) {
    if (check_color (substr($item, 2))) {
      $input_color = substr($item, 2);
      $input_color = "$bright_prefix$input_color";
    }
  } elsif (begins_with($item, $speech_flag)) {
    if (check_color (substr($item, 2))) {
      $speech_color = substr($item, 2);
      $speech_color = "$bright_prefix$speech_color";
    }
  } elsif (begins_with($item, $game_flag)) {
    if (check_color (substr($item, 2))) {
      $game_color = substr($item, 2);
      $game_color = "$bright_prefix$game_color";
    }
  } elsif (begins_with($item, $error_flag)) {
    if (length($item) gt 2) {
      if (check_color (substr($item, 2))) {
        $error_color = substr($item, 2);
        $error_color = "$bright_prefix$error_color";
      }
    }
  }
}
# define array for selected colors
my @setcolors = ($debug_color, $input_color, $speech_color, $game_color, $error_color);

# Below is where user interactions take place\
my $which_part = 0;
ROOMLOOP: while ($loop_state) {
  switch ($which_part) {
    case 0     { $which_part = area0(); }
    case 1     { $which_part = area1(); }
    case 2     { $which_part = area2(); }
    case 3     { $which_part = area3(); }
    case 4     { $which_part = area4(); }
    case 11    { print "Back to 1 first"; }
    case 12    { print "Back to 2 first"; }
    case 13    { print "Back to 3 first"; }
    case 14    { print "Back to 4 first"; }
    case 20    { print "Next to 0 again"; }
    case 21    { print "Next to 1 again"; }
    case 22    { print "Next to 2 again"; }
    case 23    { print "Next to 3 again"; }
    case 24    { print "Next to 4 again"; }
    case 30    { print "Back to 0 again"; }
    case 31    { print "Back to 1 again"; }
    case 32    { print "Back to 2 again"; }
    case 33    { print "Back to 3 again"; }
    case 34    { print "Back to 4 again"; }
    else       { $loop_state = 0; }
  }
}

# Below are the functions for each area of room
# Area 0
sub area0 {
  my $val =
'You wake up in a strange hallway. Your head is pounding with a terrible migraine as you drowsily rouse yourself. As you move, you find your headache begins to clear, and you are able to sit up relatively easily.
The first thing that you realize is that you are in a strange hallway. Not strange by way of appearance, but rather strange in that you don'."'".'t recognize it, beyond the general ordinarity of it.
The strangest feature of the hallway is the floors, which appears to be some kind of hard panelling, which is painted with blues, purples, and cyans; turquoises, ceruleans, and azures. If you stare at it for long enough, it almost seems to be moving, giving you slight motion sickness.';
  print color($game_color), "$val", color($reset), "\n";
  return 255;
}

# Area 1
sub area1 {
  my $val = '';
  print color($game_color), "$val", color($reset), "\n";
  return 255;
}

# Area 2
sub area2 {
  my $val = '';
  print color($game_color), "$val", color($reset), "\n";
  return 255;
}

# Area 3
sub area3 {
  my $val = '';
  print color($game_color), "$val", color($reset), "\n";
  return 255;
}

# Area 4
sub area4 {
  my $val = '';
  print color($game_color), "$val", color($reset), "\n";
  return 255;
}
