use Term::ANSIColor;
use Switch;

sub check_color { # Checks if the color is valid
  if (not length $_[0]) {
    return ''; # Return '' (false) if $_[0] is undefined or 0 length
  } else {
    foreach $i (@colors) { #
      if ($_[0] eq $i) {
        return '1';
      }
    }
    return '';
  }
}
sub begins_with {
    return substr($_[0], 0, length($_[1])) eq $_[1];
}

@colors = ("red", "green", "yellow", "blue", "magenta", "cyan", "white", "black");
my $colorbool = 0;
my $bright_prefix = "bright_";
my ($default, $reset) = ("white", 'reset');
my ($debug_flag, $input_flag, $speech_flag, $game_flag, $error_flag) = ("-d", "-i", "-s", "-g", "-e");
my ($debug_color, $input_color, $speech_color, $game_color, $error_color) = ($default, $default, $default, $default, $default);
my $loop_state = 1;
foreach $item (@ARGV) {
  if (begins_with($item, $debug_flag)) {
    if (check_color (substr($item, 2))) {
      $debug_color = substr($item, 2);
      $debug_color = "$bright_prefix$debug_color";
    }
  } elsif (begins_with($item, $input_flag)) {
    if (check_color (substr($item, 2))) {
      $input_color = substr($item, 2);
      $input_color = "$bright_prefix$input_color";
    }
  } elsif (begins_with($item, $speech_flag)) {
    if (check_color (substr($item, 2))) {
      $speech_color = substr($item, 2);
      $speech_color = "$bright_prefix$speech_color";
    }
  } elsif (begins_with($item, $game_flag)) {
    if (check_color (substr($item, 2))) {
      $game_color = substr($item, 2);
      $game_color = "$bright_prefix$game_color";
    }
  } elsif (begins_with($item, $error_flag)) {
    if (length($item) gt 2) {
      if (check_color (substr($item, 2))) {
        $error_color = substr($item, 2);
        $error_color = "$bright_prefix$error_color";
      }
    }
  }
}
# define array for selected colors
my @setcolors = ($debug_color, $input_color, $speech_color, $game_color, $error_color);

print "Loaded Room 1\n";
