#include <EXTERN.h> // needed for embedding perl
#include <perl.h> // needed for embedding perl
#include <XSUB.h>
#include <stdio.h>
#include "colors.h" // this defines the escape codes for colors
#include "config.h" // this is where hard-coded configuration is done
#define SUBROUTINE "load" // this is the subroutine to be run

#define NAME "The Infinite Hallway" // App name
#define AUTH "Zachary Smith" // Author name

// The following is from perlembed, allows dynamic library loading
static void xs_init (pTHX);
EXTERN_C void boot_DynaLoader (pTHX_ CV* cv);
EXTERN_C void xs_init(pTHX) {
       char *file = __FILE__;
       /* DynaLoader is a special case */
       newXS("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);
}

static PerlInterpreter *my_perl, *my_perl2; // This is the perl interpreter

int main(int argc, char **argv, char **env) {
  // paths to the perl modules
  char *room0[] = { "", "modules/room0.pl", "-e", "-gcyan", "-dmagenta", "-igreen", "-scyan", "-ered", "0", NULL };
  char *room1[] = { "", "modules/room1.pl", "-e", "-gcyan", "-dmagenta", "-igreen", "-scyan", "-ered", "0", NULL };

  int roomload = 254;
  char read[5];

  printf("%s%s by %s%s\n", CYAN, NAME, AUTH, CLEAR); // prints the title and author
// The following is from the perldocs on embedding
  PERL_SYS_INIT3(&argc,&argv,&env);
  my_perl = perl_alloc();
  perl_construct(my_perl);
  PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
// v-- all perl commands are done between here
  while (1) {
    printf("%sWelcome! Type 0 to load the hallway, 1 to load room 1, or q to quit.%s\n", CYAN, CLEAR);
    fgets(read, 5, stdin);
    roomload = atoi(read);
    if (!strncmp(read, "q", 1)) {
      roomload = 255;
    }
    switch (roomload) {
      case 0:
	      if (strcmp(read,"0\n")) {
	        printf("Please input a number (0, 1), or 'q' to quit!\n");
	      } else {
          printf("%s---LOADING-ROOM-0---%s\n", YELLOW, CLEAR);
	        perl_parse(my_perl, xs_init, 10, room0, env);
	        perl_run(my_perl);
          printf("%s---LEAVING-ROOM-0---%s\n", YELLOW, CLEAR);
	      }
        break;
      case 1:
        printf("%s---LOADING-ROOM-1---%s\n", YELLOW, CLEAR);
        perl_parse(my_perl, xs_init, 10, room1, env);
        perl_run(my_perl);
        printf("%s---LEAVING-ROOM-1---%s\n", YELLOW, CLEAR);
        break;
      case 255: printf("%sGoodbye!%s\n", CYAN, CLEAR);
        exit(EXIT_SUCCESS);
      default:
        printf("Please input a number (0, 1), or 'q' to quit!\n");
        break;
    }
  }
// ^-- and here
// The following is from the perldocs on embedding
  perl_destruct(my_perl);
  perl_free(my_perl);
  PERL_SYS_TERM();
  exit(EXIT_SUCCESS);
}
