CCOMP=cc
OFLAG=-O2 -lpthread -lperl -lm `perl -MExtUtils::Embed -e ccopts` -o
BINNM=tih
SRCDR=src
MODDR=modules
SOURCES=src/main.c
all: $(SOURCES)
	$(CCOMP) $(OFLAG) $(BINNM) $(SRCDR)/main.c

clean:
	rm -rf $(BINNM)
